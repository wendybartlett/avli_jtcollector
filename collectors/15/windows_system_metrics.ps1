# Update output buffer size to prevent clipping of metrics in java input stream.
if( $Host -and $Host.UI -and $Host.UI.RawUI ) {
  $rawUI = $Host.UI.RawUI
  $oldSize = $rawUI.BufferSize
  $typeName = $oldSize.GetType( ).FullName
  $newSize = New-Object $typeName (500, $oldSize.Height)
  $rawUI.BufferSize = $newSize
}

$processor = (Get-Counter -listset processor).paths
$physicalDisk = (Get-Counter -listset physicaldisk).paths
$memory = (Get-Counter -listset memory).paths
$network = (Get-Counter -listset 'network interface').paths
$mycounters = $processor + $physicalDisk + $memory + $network

$perf = Get-Counter -Counter $mycounters
$epoch = Get-Date -UFormat "%s"
$epoch = [int] $epoch

$hash = @{}
$perf.CounterSamples | foreach {
$key = $_.path -replace "\\\\", "" | foreach { $_ -replace "\\", "."} | foreach { $_ -replace " ", "_"} | foreach { $_ -replace "\(", "."} | foreach { $_ -replace "\)", ""} | foreach { $_ -replace "/", "_per_"} | foreach { $_ -replace "\%", "percent"} | foreach { $_ -replace "\:", ""} | foreach { $_ -replace "\[", "."} | foreach { $_ -replace "\]", "."};
$value = $_.CookedValue
$hash[$key]=$value
}

$hash.GetEnumerator() | % { 
    $key = $($_.key)
    $thishost = $key.Split(".")[0]
    $metric = $key -replace "$thishost", ""
    $val = [long] $($_.value)
    "tts.jtcollector.windows$metric $epoch $val"
}

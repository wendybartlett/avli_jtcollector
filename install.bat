﻿SET JT_PATH=c:\jtcollector
SET JT_SERVICE_NAME=jtcollector
SET JT_JAR=out\artifacts\avli_jtcollector_jar\avli_jtcollector.jar
SET START_CLASS=com.tts.jtcollector.JTCollector
SET STOP_CLASS=com.tts.jtcollector.JTCollector
SET START_METHOD=main
SET STOP_METHOD=shutdown
rem ; separated values
SET START_PARAMS=%JT_PATH%\avli_jtcollector.win.properties
rem ; separated values
SET JVM_OPTIONS=-Dlog4j.configuration=file:%JT_PATH%\avli_jtcollector.log4j.properties

c:\jtcollector\lib\jtcollector.exe //IS//%JT_SERVICE_NAME% --Install="%JT_PATH%\lib\jtcollector.exe" --Description="JTCollector" --Jvm=auto --Startup=auto --StartMode=jvm --StartClass=%START_CLASS% --StartMethod=%START_METHOD% ++StartParams="%START_PARAMS%" --StopMode=jvm --StopClass=%STOP_CLASS% --StopMethod=%STOP_METHOD% --Classpath="%JT_PATH%\%JT_JAR%" --DisplayName="%JT_SERVICE_NAME%" --JvmOptions="%JVM_OPTIONS%" --LogLevel=debug --LogPrefix=jtcollector --LogPath="c:\logs"
net start jtcollector

package com.tts.util;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Hashtable;

/**
 * Created with IntelliJ IDEA.
 * User: eric
 * Date: 7/27/13
 * Time: 6:01 PM
 *
 * This class contains a:
 *      schema cache
 *      message decoder
 *      message encoder
 *
 */
public class AvroSchemaSystem {
    static Logger log = Logger.getLogger(AvroSchemaSystem.class.getName());

    // Schema Repo properties
    private String schema_repo_host                 = null;
    private String schema_repo_port                 = null; /* port to listen on */
    private String delimiter                        = null;
    private Hashtable<String, Schema> schema_cache  = null; // cache of schemas
    private String schemaPath                       = null;

    public AvroSchemaSystem(String host, String port, String prefixDelimiter, String schemaRepoPath) {
        schema_cache = new Hashtable<String, Schema>();
        delimiter           = prefixDelimiter;
        schema_repo_host    = host;
        schema_repo_port    = port;
        schemaPath          = schemaRepoPath;
    }

    // Returns true if the given schema was retrieved from the Schema Repo and cached
    public boolean init(String topic, String version) {
        Schema schema = getSchema(topic, version);
        if (schema != null) {
            return true;
        } else {
            log.error("Unable to retrieve schema: " + topic + " version: " + version);
            return false;
        }
    }

    // Looks up the schema url created by topic and version in the schema cache and returns the schema
    // If not found or if we can not contact the schema repo return null
    public Schema getSchema(String topic, String version) {
        String schema_url = "http://" + schema_repo_host + ":" + schema_repo_port + schemaPath + version;
        Schema schema;
        try {
            schema = schema_cache.get(schema_url);
            if (schema == null) {
                Schema.Parser parser = new Schema.Parser();
                Client client = Client.create();
                WebResource webResource = client.resource(schema_url);
                String ret = webResource.get(String.class);
                schema = parser.parse(ret);
                schema_cache.put(schema_url, schema);
            }
        } catch (Exception e) {
            //   send a request to the schema repo to get the schema
            log.error("Unable to retrieve schema.", e);
            schema = null;
        }
        return schema;
    }

    // decode the Avro encoded message
    private GenericRecord avroDecodeMessage(Schema writer_schema, Schema reader_schema, byte[] message) throws IOException {
        // Create String buffer
        ByteArrayInputStream inputStream = new ByteArrayInputStream(message);
        // Create DatumReader
        DatumReader<GenericRecord> reader = new GenericDatumReader<GenericRecord>(writer_schema, reader_schema);
        // Create Decoder
        Decoder decoder = DecoderFactory.get().binaryDecoder(inputStream, null);
        return reader.read(null, decoder);
    }

    // decodes the message body which is base64 encoded
    public GenericRecord decodeKafkaMessage(byte[] message, String topic, String reader_version, boolean base64) throws IOException {
        GenericRecord decoded;

        // Get writer reader
        Schema reader_schema = getSchema(topic, reader_version);
        if (reader_schema == null) {
            log.error("Unable to get writer schema from Avro Schema Repo");
            return null;
        }

        Schema writer_schema;

        int len = message.length;

        // Parse schema version from message body
        int msg_start = 0;
        while (msg_start < len && message[msg_start] != ':') {
            msg_start++;
        }
        // Get the writers schema version id as a string
        String writer_schema_id = new String(message, 0, msg_start);
        msg_start += 2;
        // Get a byte buffer that is just the message body
        byte [] msg_content = Arrays.copyOfRange(message, msg_start, len);

        // Try to get the schema this message was encoded with
        try {
            writer_schema = getSchema(topic, writer_schema_id);
        } catch (Exception e) {
            log.error("Cant get or parse writer schema.", e);
            return null;
        }

        // Did we get a schema
        if (writer_schema == null) {
            log.error("Cant get or parse writer schema.");
            return null;
        }

        if (base64) {
            // Base64 decode message
            byte[] b64_decoded = Base64.decodeBase64(msg_content);
            decoded = avroDecodeMessage(writer_schema, reader_schema, b64_decoded);
        } else {
            decoded = avroDecodeMessage(writer_schema, reader_schema, msg_content);
        }
        return decoded;
    }

    public byte[] encodeKafkaMessage(GenericRecord record, String topic, String version, boolean base64) throws IOException {

        Schema writer_schema = getSchema(topic, version);
        if (writer_schema == null) {
            log.error("Unable to get writer schema: " + topic + " version: " + version);
            return null;
        }

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        GenericDatumWriter<GenericRecord> writer = new GenericDatumWriter<GenericRecord>(writer_schema);
        Encoder encoder = EncoderFactory.get().binaryEncoder(bao, null);
        writer.write(record, encoder);
        encoder.flush();

        byte[] encoded;
        if (base64)
            encoded = Base64.encodeBase64(bao.toByteArray());
        else
            encoded = bao.toByteArray();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        byte[] prefix = (version + delimiter).getBytes();
        outputStream.write(prefix);
        outputStream.write(encoded);

        return outputStream.toByteArray();
    }

}
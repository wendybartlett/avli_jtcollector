package com.tts.jtcollector;

import org.apache.log4j.Logger;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.message.MessageAndMetadata;
import org.apache.avro.generic.GenericRecord;
import org.apache.log4j.Logger;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: wendybartlett
 * Date: 10/9/13
 * Time: 9:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class JTCollectorMetricsCollector extends Thread {
    static Logger log                       = Logger.getLogger(JTCollectorMetricsCollector.class.getName());
    // Set true when the entire service should be shutdown
    public static boolean time_to_shutdown  = false;
    private String local_hostname           = null;
    private static int max_count            = 0;

    public JTCollectorMetricsCollector(int send_time) {
        max_count = send_time / 100;
    }

    public void freeResources() {
        time_to_shutdown = true;
    }

    public void run() {
        log.info("Starting worker, metrics collector.");
        int count = 1;

        while (!time_to_shutdown) {
            if (count == 0) {
                try {

                    StringBuilder out = new StringBuilder();
                    long epoch = System.currentTimeMillis()/1000;

                    out.append("tts.jtcollector.avro_encode_failures ").append(epoch).append(" ").append(Long.toString(JTCollector.avroEncodeFailure.get()));
                    MetricSender.sendMessage(out.toString());
                    out.setLength(0);

                    out.append("tts.jtcollector.number_of_metrics_sent ").append(epoch).append(" ").append(Long.toString(JTCollector.numMessagesSent.get()));
                    MetricSender.sendMessage(out.toString());
                    out.setLength(0);

                } catch (Exception e) {
                    log.error("Something happended: ", e);
                }
            }

            try {
                sleep(100);
            } catch (InterruptedException e) {
                time_to_shutdown = true;
            }

            count++;
            if (count > max_count) {
                count = 0;
            }
        }
    }
}



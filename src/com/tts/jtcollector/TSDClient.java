package com.tts.jtcollector;

import org.apache.log4j.Logger;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wendybartlett
 * Date: 10/16/13
 * Time: 11:06 AM
 * To change this template use File | Settings | File Templates.
 */

public class TSDClient extends Thread {
    static Logger log                   = Logger.getLogger(TSDClient.class.getName());
    private Socket opentsdb             = null;
    private String tsdHost              = null;
    private int tsdPort                 = 0;
    private int sleepSeconds            = 0;
    private boolean time_to_shutdown    = false;
    private List<String> tsdMetricList;

    public TSDClient(String tsdHost, String tsdPort, int sleepSeconds, List<String> tsdMetricList) {
        this.tsdHost        = tsdHost;
        this.tsdPort        = Integer.parseInt(tsdPort);
        this.sleepSeconds   = sleepSeconds;
        this.tsdMetricList  = tsdMetricList;
    }

    public void createTSDConn() throws IOException {
        opentsdb        = new Socket(tsdHost, tsdPort);
    }

    public void closeTSDConn(Socket socket) throws IOException {
        opentsdb.close();
    }

    public void sendToTSD(StringBuilder bulkData, int numRetries) {
        int retries             = 0;
        boolean dataSent        = false;
        BufferedWriter tsdOut   = null;

        if (bulkData.length() > 0) {
            // We have data, create socket and send
            while (retries < numRetries && (!dataSent)) {
                try {
                    // Create TSD connection
                    createTSDConn();
                    if (opentsdb != null) {
                        tsdOut = new BufferedWriter(new OutputStreamWriter(opentsdb.getOutputStream()), 64 * 1024);
                        if (tsdOut != null) {
                            tsdOut.write(bulkData.toString());
                            tsdOut.flush();
                            tsdOut.close();
                            dataSent = true;
                        }
                    } else {
                        retries++;
                    }
                    // Close TSD connection
                    closeTSDConn(opentsdb);
                } catch (IOException e) {
                    retries++;
                    if (retries == numRetries) {
                        log.error("Unable to send metrics to TSD after: " + numRetries, e);
                    }
                }
            }
        } else {
            try {
                sleep(500);
            } catch (InterruptedException e) {
                time_to_shutdown = true;
            }
        }
    }

    public void freeResources() {
        time_to_shutdown = true;
    }

    public void run() {
        while(!time_to_shutdown) {
            boolean metricsWaiting;
            StringBuilder bulk_data  = new StringBuilder();

            synchronized (tsdMetricList) {
                if (tsdMetricList.size() > 0)
                    metricsWaiting = true;
                else
                    metricsWaiting = false;
            }
            if (metricsWaiting) {
                synchronized (tsdMetricList) {
                    for (String metric : tsdMetricList) {
                        String str = "put " + metric + "\n";
                        bulk_data.append(str);
                        JTCollector.numMessagesSent.incrementAndGet();
                    }
                    tsdMetricList.clear();
                }
            }
            sendToTSD(bulk_data, 3);
            try {
                sleep(sleepSeconds * 1000);
            } catch (InterruptedException e) {
                time_to_shutdown = true;
            }
        }
    }
}

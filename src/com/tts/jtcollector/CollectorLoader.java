package com.tts.jtcollector;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: wendybartlett
 * Date: 10/7/13
 * Time: 4:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class CollectorLoader extends Thread {
    private String directory;
    static Logger log           = Logger.getLogger(JTCollector.class.getName());
    private int loaderWaitTime  = 300000;
    private List<String> collectorsList;
    private List<String> runningCollectorsList;
    private List<String> doNotRestartList;
    private boolean time_to_shutdown    = false;

    public CollectorLoader(String collectorDirectory, int loaderWait, List<String> listOfCollectors, List<String> listOfRunningCollectors, List<String> listToNotRestart) {
        directory               = collectorDirectory;
        collectorsList          = listOfCollectors;
        runningCollectorsList   = listOfRunningCollectors;
        loaderWaitTime          = loaderWait;
        doNotRestartList        = listToNotRestart;
    }

    public static boolean isNumeric(String str)
    {
        try
        {
            int d = Integer.parseInt(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    public void freeResources() {
        time_to_shutdown = true;
    }

    public void run() {
        while (!time_to_shutdown) {
            File dir = new File(directory);

            List<File> files = (List<File>) FileUtils.listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
            for (File file : files) {
                String pattern = Pattern.quote(System.getProperty("file.separator"));
                String[] parts = file.getPath().split(pattern);
                String collectorTimeDenomination = parts[parts.length - 2];
                if (!file.isDirectory() && isNumeric(collectorTimeDenomination)) {
                    boolean loaded_exists;
                    boolean running_exists;
                    boolean do_not_restart_exists;
                    // Check list to see if the collector is already known
                    synchronized (collectorsList) {
                        loaded_exists = collectorsList.contains(file.getAbsolutePath());
                    }
                    // Check list to see if the collector is already running
                    synchronized (runningCollectorsList) {
                        running_exists = runningCollectorsList.contains(file.getAbsolutePath());
                    }
                    // Check list to see if the collector has decommissioned itself by request
                    synchronized (doNotRestartList) {
                        do_not_restart_exists = doNotRestartList.contains(file.getAbsolutePath());
                    }
                    // If the collector is not already in the list, is not already running, and has not decommissioned itself, we'll start it.
                    if (!loaded_exists && !running_exists && !do_not_restart_exists) {
                        synchronized (collectorsList) {
                            collectorsList.add(file.getAbsolutePath());
                        }
//                        System.out.println("New collector added - " + file.getAbsolutePath());
                        log.info("New collector added - " + file.getAbsolutePath());
                    }
                }
            }
            // Sleep until next iteration
            try {
                sleep(loaderWaitTime);
            } catch (InterruptedException e) {
                time_to_shutdown = true;
            }
        }
    }
}
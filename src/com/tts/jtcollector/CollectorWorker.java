package com.tts.jtcollector;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: wendybartlett
 * Date: 10/8/13
 * Time: 10:41 AM
 * To change this template use File | Settings | File Templates.
 */
public class CollectorWorker extends Thread {
    private List<String> collectorsList;
    private List<String> metricList;
    private List<String> runningCollectorsList;
    private List<String> doNotRestartList;
    static Logger log                   = Logger.getLogger(JTCollector.class.getName());
    private String collPath             = "";
    private boolean time_to_shutdown    = false;

    public CollectorWorker(List<String> listOfCollectors, List<String> listOfMetrics, List<String> listOfRunningCollectors, List<String> listToNotRestart, String collectorsPath) {
        collectorsList          = listOfCollectors;
        metricList              = listOfMetrics;
        runningCollectorsList   = listOfRunningCollectors;
        collPath                = collectorsPath;
        doNotRestartList        = listToNotRestart;
    }

    public ArrayList<String> getLaunchProcess(String collector, String collectorFilename) throws IOException {
        String[] parts              = collectorFilename.split("\\.");
        ArrayList<String> params            = new ArrayList<String>();

        if (parts[parts.length - 1].equals("py")) {
            params.add("python");
            params.add(collector);
        } else if ((parts[parts.length - 1].equals("bat"))) {
            params.add("cmd");
            params.add("/c");
            params.add("\"" + collector + "\"");
        } else if (parts[parts.length - 1].equals("ps1")) {
            params.add("powershell");
            params.add("-ExecutionPolicy");
            params.add("RemoteSigned");
            params.add("-noprofile");
            params.add("-noninteractive");
            params.add("-file");
            params.add(collector);
        } else {
            params.add(collector);
        }

        return params;
    }

    public void freeResources() {
        time_to_shutdown = true;
    }

    public void decommissionCollector(String collector) {
        synchronized (runningCollectorsList) {
            doNotRestartList.add(collector);
        }
        synchronized (runningCollectorsList) {
            runningCollectorsList.remove(collector);
        }
        log.info("Removed collector: " + collector);
    }

    public void launchCollector(String collector, int collectorRunInterval, List<String> runningCollectorsList) throws IOException, InterruptedException {
        String pattern                      = Pattern.quote(System.getProperty("file.separator"));
        String sep                          = Pattern.quote(System.getProperty("line.separator"));

        String[] collectorFilenameParts     = collector.substring(1).split(pattern);
        String collectorFilename            = collectorFilenameParts[collectorFilenameParts.length - 1];
        boolean shouldSleep                 = false;
        StringBuilder results               = new StringBuilder();
        Process process                     = null;
        ArrayList<String> params            = new ArrayList<String>();
        int exitVal                         = 0;
        boolean time_to_shutdown            = false;
        int launchCounter                   = 0;

        if (collectorRunInterval != 0) {
            shouldSleep = true;
        }

        while(!time_to_shutdown) {
            try {
                log.trace("Running collector: " + collector);
                params                  = getLaunchProcess(collector, collectorFilename);
                ProcessBuilder pb       = new ProcessBuilder(params);
                pb.directory(new File(collPath));
                process                 = pb.start();
                process.getOutputStream().close();
                BufferedReader br_err   = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                String line;

                Scanner sMain = new Scanner(process.getInputStream(),"UTF-8").useDelimiter(sep);
                Pattern wordPattern = Pattern.compile("~");
                while (sMain.hasNext()) {
                    line = sMain.next();
                    //Dedup metrics
                    boolean exists;
                    synchronized (collectorsList) {
                        exists = metricList.contains(line);
                    }
                    if (!exists)
                        metricList.add(line);
                }
                sMain.close();
                StringBuilder sb = new StringBuilder();
                while ((line = br_err.readLine()) != null) {
                    sb.append(line + "\n");
                }
                log.trace(sb.toString());
                br_err.close();
                exitVal = process.exitValue();
//                System.out.println("Exit Value: " + exitVal);

                // Check for collector exit value of 13, if 13 decommission collector
                if (exitVal == 13) {
                    synchronized (runningCollectorsList) {
                        doNotRestartList.add(collector);
                    }
                    log.info("Removing: " + collector + "from the list of collectors (by request)");
                    time_to_shutdown = true;
                } else if (sb.toString().length() > 0) {
                    launchCounter++;
                    if (launchCounter == 3) {
                        log.error("Failed to run collector after 3 attempts - decommissioning!", new Throwable(sb.toString()));
                        decommissionCollector(collector);
                        time_to_shutdown = true;
                    }
                    sleep(30000);
                } else if (shouldSleep) {
                    try {
                        sleep(collectorRunInterval * 1000);
                        log.trace("Sleeping for appropriate interval..." + collectorRunInterval);
                    } catch (InterruptedException e) {
                        time_to_shutdown = true;
                    }
                }
            } catch (IOException e) {
                launchCounter++;
                if (launchCounter == 3) {
                    log.error("Failed to run collector after 3 attempts - decommissioning!", e);
                    decommissionCollector(collector);
                    time_to_shutdown = true;
                }
                sleep(30000);
            } catch (InterruptedException e) {
                time_to_shutdown = true;
            } catch (Exception e) {
                launchCounter++;
                if (launchCounter == 3) {
                    log.error("Failed to run collector after 3 attempts - decommissioning!", e);
                    decommissionCollector(collector);
                    time_to_shutdown = true;
                }
                sleep(30000);
            }
        }
    }

    public int getCollectorRunInterval (String collectorPath) {
        int inc         = 0;
        String pattern  = Pattern.quote(System.getProperty("file.separator"));
        String[] parts  = collectorPath.split(pattern);
        inc             = Integer.parseInt(parts[parts.length - 2]);
        return inc;
    }

    public void run() {
        while(!time_to_shutdown) {
            boolean collectorsWaiting;

            synchronized (collectorsList) {
                if (collectorsList.size() > 0)
                    collectorsWaiting = true;
                else
                    collectorsWaiting = false;
            }

            if (collectorsWaiting) {
                String currentCollector     = "";

                synchronized (collectorsList) {
                    for (String coll : collectorsList) {
                        currentCollector    = coll;

                        collectorsList.remove(currentCollector);
                        if (currentCollector.length() > 0) {
                            break;
                        }
                    }
                }
                boolean running = false;
                // Add worker to running collectors
                synchronized (runningCollectorsList) {
                    running = runningCollectorsList.contains(currentCollector);
                }
                if (!running) {
                    synchronized (runningCollectorsList) {
                        runningCollectorsList.add(currentCollector);
                    }
//                    System.out.println("Running Collector: " + currentCollector);
                    log.info("Running Collector: " + currentCollector);
                }
                // Got valid collector, let's do some setup and kick it off
                if (currentCollector.length() > 0) {
                    int collectorRunInterval = getCollectorRunInterval(currentCollector);
                    try {
                        launchCollector(currentCollector, collectorRunInterval, runningCollectorsList);
                    } catch (IOException e) {
                        log.error(e);
                    } catch (InterruptedException e) {
                        log.error(e);
                    }
                }
            } else {
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
    }
}

set JT_PATH=%CD%
SET JT_SERVICE_NAME=JTCollector
SET JT_JAR=avli_tcollector.jar
SET START_CLASS=com.tts.jtcollector.JTCollector
SET START_METHOD=main
rem ; separated values
SET START_PARAMS=%JT_PATH%\avli_jtcollector.win.properties
rem ; separated values
SET JVM_OPTIONS=-Dapp.home=%JT_PATH%;-Dlog4j.configuration=file:%JT_PATH%\avli_jtcollector.log4j.properties
net stop %JT_SERVICE_NAME%
jtcollector.exe //US//%JT_SERVICE_NAME% --Install="%JT_PATH%\jtcollector.exe" --Jvm=auto --Startup=auto --StartMode=jvm --StartClass=%START_CLASS% --StartMethod=%START_METHOD% --StartParams=%START_PARAMS% --StopMode=jvm --Classpath="%JT_PATH%\%JT_JAR%" --DisplayName="%JT_SERVICE_NAME%" ++JvmOptions=%JVM_OPTIONS%
net start %JT_SERVICE_NAME%
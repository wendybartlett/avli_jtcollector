#!/bin/bash

if [ $# -lt 2 ];
then
	echo "USAGE: $0 -config /etc/jtcollector/avli_jtcollector.properties"
	exit 1
fi

export JVM_OPTS="-Xmx1G -server  -Dlog4j.configuration=file:/etc/jtcollector/avli_jtcollector.log4j.properties"

export JMX_PORT=${JMX_PORT:-9243}

$(dirname $0)/jtcollector-run-class.sh com.tts.jtcollector.JTCollector $@